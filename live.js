function secondsTillSessionStarted() {
    const timeOfTheLastStartedSessionElt = document.getElementById('spanCurrentStart');
    let dateNow = new Date;
    let dateOfLastStartedSession = new Date;
    let splittedTimeOfTheLastStartedSession = timeOfTheLastStartedSessionElt.innerHTML.split(' ');
    let splitHour = splittedTimeOfTheLastStartedSession[0].split(':');
    let hours = splitHour[0];
    let minutes = splitHour[1];
    dateOfLastStartedSession.setHours(parseInt(hours));
    dateOfLastStartedSession.setMinutes(parseInt(minutes));
    dateOfLastStartedSession.setSeconds(0);
    let diff = dateNow.getTime() - dateOfLastStartedSession.getTime();
    let totalSecondsTillLastStartedSession = diff / 1000;
    return parseInt(totalSecondsTillLastStartedSession);
}

/* Seconds History Getter */
function getHistorySeconds(){
    let totalHistorySeconds = 0;
    if (document.getElementById('ulSessionsDay')){
        if (document.getElementById('ulSessionsDay').getElementsByTagName('li')) {
            const timeClockDayHistoryListElt = document.getElementById('ulSessionsDay').getElementsByTagName('li');
            for (let i = 0; i < timeClockDayHistoryListElt.length; i++){
                const historyEltInnerHtml = timeClockDayHistoryListElt[i].innerHTML;
                const splitedHistoryArray = historyEltInnerHtml.split(' ');
                const hourHistory = splitedHistoryArray[5];
                const minuteHistory = splitedHistoryArray[8];
                totalHistorySeconds += (((parseInt(hourHistory) * 60) * 60) + (parseInt(minuteHistory) * 60));
            }
        }
    }
    return totalHistorySeconds;
}

function getHistorySecondsOfWeek(time){
    let result = secondsToHms(time);
    if (document.getElementById('spanTotalSemaine')) {
        const weekTimeElt = document.getElementById('spanTotalSemaine');
        let splittedHistoryArray = weekTimeElt.innerHTML.split(' ');
        if ((splittedHistoryArray.includes('heures') || splittedHistoryArray.includes('heure')) && (splittedHistoryArray.includes('minutes') || splittedHistoryArray.includes('minute'))){
            let hours = splittedHistoryArray[0];
            let minutes = splittedHistoryArray[3];
            let totalSeconds = (parseInt(hours) * 3600) + (parseInt(minutes) * 60);
            result = secondsToHms(totalSeconds + time);
        }
    }
    return result;
}

/* Seconds Converter to Human timer string */
function secondsToHms(d) {
    d = Number(d);
    let h = Math.floor(d / 3600);
    let m = Math.floor(d % 3600 / 60);
    let s = Math.floor(d % 3600 % 60);

    let hDisplay = h > 0 ? h + " H " : "";
    let mDisplay = m > 0 ? m + " m " : "";
    let sDisplay = s > 0 ? s + " s " : "";
    return hDisplay + mDisplay + sDisplay;
}

function getGreenToRed(time){
    let percent = getPercentOfDay(time);
    if (percent >= 100){
        return 'rgb(0,117,219)';
    }
    r = percent<50 ? 200 : Math.floor(255-(percent*2-100)*255/100);
    g = percent>50 ? 200 : Math.floor((percent*2)*255/100);
    return 'rgb('+r+','+g+',0)';
}

function getHoursToDoToday(){
    let date = new Date;
    let today = date.getDay();
    let hoursToDo = 0;
    if (today === 5){
        hoursToDo = 7;
    } else {
        hoursToDo = 8;
    }
    return parseInt(hoursToDo);
}

function getPercentOfDay(time) {
    let hoursToDo = getHoursToDoToday();
    let percentOfDay;
    percentOfDay = (time * 100) / (hoursToDo * 3600);
    if (percentOfDay > 100){
        percentOfDay = 100;
    }
    return percentOfDay.toFixed(0);
}

function endOfDayTime(time) {
    let timeEndOfDay = new Date;
    let hoursToDo = getHoursToDoToday();
    let secondsTodo = (hoursToDo + 2) * 3600;
    let toDo = parseInt(secondsTodo) - parseInt(time);
    timeEndOfDay.setSeconds(timeEndOfDay.getSeconds() + toDo);
    let res = timeEndOfDay.toISOString().slice(11,19);
    if (time > (hoursToDo * 3600)) {
        return `${res} - T'es une machine...`;
    }
    return res;
}

function timeToEndOfDay(time) {
    let hoursToDo = getHoursToDoToday();
    let restTime = (hoursToDo * 3600) - time;
    let result;
    if (restTime <= 0) {
        restTime = restTime * -1;
        result = `BONUS : ${secondsToHms(restTime)}`;
    } else {
        result = `RESTE : ${secondsToHms(restTime)}`;
    }
    return result;
}

/* Live Timer */
function timer(el) {
    let time = getHistorySeconds() + secondsTillSessionStarted();
    el = document.getElementsByClassName(el)[0];
    let timer = document.createElement('div');
    timer.style.width = 'auto';
    timer.style.position = 'relative';
    timer.style.height = '62px';
    timer.style.padding = '20px';
    timer.style.margin = '20px';
    timer.style.border = '1px solid #000';
    timer.style.marginBottom = '40px';

    let clock = document.createElement('div');
    let progressionBar = document.createElement('div');
    let endOfDay = document.createElement('div');
    let totalWeek = document.createElement('div');
    function refresh() {
        time += 1;
        clock.innerHTML = secondsToHms(time);
        clock.style.fontSize = '1.5em';
        clock.style.left = '0px';
        clock.style.top = '0px';
        clock.style.fontWeight = 'bold';
        clock.style.color = getGreenToRed(time);
        clock.style.textAlign = 'center';
        clock.style.height = '40px';
        clock.style.display = 'inline-block';
        clock.style.width = '100%';
        clock.style.backgroundColor = 'black';
        clock.style.zIndex = '1';
        clock.style.position = 'absolute';
        clock.style.marginBottom = '150px';

        progressionBar.innerHTML = `${timeToEndOfDay(time)}`;
        progressionBar.style.textAlign = 'right';
        progressionBar.style.paddingRight = '10px';
        progressionBar.style.fontWeight = 'bold';
        progressionBar.style.color = '#000000';
        progressionBar.style.whiteSpace = 'nowrap';
        progressionBar.style.overflow = 'visible';
        progressionBar.style.width = `${getPercentOfDay(time)}%`;
        progressionBar.style.height = '20px';
        progressionBar.style.zIndex = '2';
        progressionBar.style.position = 'absolute';
        progressionBar.style.left = '0px';
        progressionBar.style.top = '40px';
        progressionBar.style.backgroundColor = getGreenToRed(time);

        endOfDay.innerHTML = `Fin de journee sans pause : ${endOfDayTime(time)}`;
        endOfDay.style.color = '#000000';
        endOfDay.style.textAlign = 'center';
        endOfDay.style.width = '100%';
        endOfDay.style.height = '20px';
        endOfDay.style.position = 'absolute';
        endOfDay.style.left = '0px';
        endOfDay.style.top = '60px';

        totalWeek.innerHTML = `Total de semaine : ${getHistorySecondsOfWeek(time)}`;
        totalWeek.style.position = 'absolute';
        totalWeek.style.left = '0px';
        totalWeek.style.fontWeight = 'bold';
        totalWeek.style.marginTop = '60px';
        totalWeek.style.width = '100%';
        totalWeek.style.textAlign = 'center';

    }
    timer.appendChild(progressionBar);
    timer.appendChild(clock);
    timer.appendChild(endOfDay);
    timer.appendChild(totalWeek);
    let elementToFixClock = document.getElementsByClassName('card-head')[0];
    el.insertBefore(timer, elementToFixClock);
    refresh();
    setInterval(refresh, 1000);
}

setTimeout(()=>{
    let sessionStartedElt = document.getElementById('divSessionStarted');
    if (sessionStartedElt.style.display !== 'none'){
        timer("card");
    }
}, 1000);